// How to require a file
require('./modules/script.js');

// How to require a file you only need in certain circumstances
if (document.querySelector('.test--class')) {
  require('./modules/script.js');
}
